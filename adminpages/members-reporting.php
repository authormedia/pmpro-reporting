<?php
	//vars
	global $wpdb;

	//initialize request variables
	$l = false;
	if(isset($_REQUEST['l']) and !empty($_REQUEST['l'])){$l = $_REQUEST['l'];}

	$p = false;
	if(isset($_REQUEST['p']) and !empty($_REQUEST['p'])){$p = $_REQUEST['p'];}

	$a = false;
	if(isset($_REQUEST['a']) and !empty($_REQUEST['a'])){$a = $_REQUEST['a'];}

	$c = false;
	if(isset($_REQUEST['c']) and !empty($_REQUEST['c'])){$c = $_REQUEST['c'];}

	$o = array('tmem'=>true,'nmem'=>true,'cmem'=>true);
	if(isset($_REQUEST['o']) and !empty($_REQUEST['o'])){$o = $_REQUEST['o'];}

	$s = ''.((idate('m')+5)%12+1).'/01/'.(idate('Y') + floor((idate('m')-6)/12));
	if(isset($_REQUEST['s']) and !empty($_REQUEST['s'])){$s = $_REQUEST['s'];}

	$e = ''.((idate('m')+5)%12+1).'/01/'.(idate('Y') + floor((idate('m')+6)/12));
	if(isset($_REQUEST['e']) and !empty($_REQUEST['e'])){$e = $_REQUEST['e'];}
?>
<div class="wrap pmpro_admin">
	<div class="pmpro_banner">
		<a class="pmpro_logo" title="Paid Memberships Pro - Membership Plugin for WordPress" target="_blank" href="<?php echo pmpro_https_filter("http://www.paidmembershipspro.com")?>"><img src="<?php echo PMPRO_URL?>/images/PaidMembershipsPro.gif" width="350" height="45" border="0" alt="Paid Memberships Pro(c) - All Rights Reserved" /></a>
		<div class="pmpro_tagline">Membership Plugin for WordPress</div>
		
		<div class="pmpro_meta"><a href="<?php echo pmpro_https_filter("http://www.paidmembershipspro.com")?>">Plugin Support</a> | <a href="http://www.paidmembershipspro.com/forums/">User Forum</a> | <strong>Version <?php echo PMPRO_VERSION?></strong></div>
	</div>
	<br style="clear:both;" />

	<h3 class="nav-tab-wrapper">
		<a href="admin.php?page=pmpro-revenue-reporting" class="nav-tab<?php if($_REQUEST['page'] == 'pmpro-revenue-reporting') { ?> nav-tab-active<?php } ?>">Revenue</a>
		<a href="admin.php?page=pmpro-members-reporting" class="nav-tab<?php if($_REQUEST['page'] == 'pmpro-members-reporting') { ?> nav-tab-active<?php } ?>">Membership</a>	
	</h3>

	<form id="posts-filter" method="get" action="">
	<input type="hidden" name="page" value="pmpro-members-reporting" />
	<h2>
		Membership Reporting
	</h2>

	<?php
		/*$user_query = (isset($l) and $l!==false) ? "users.membership_id = ".intval($l) : "";

		//total number of users in the user query
		$sqlQuery = "SELECT COUNT(*) FROM $wpdb->pmpro_memberships_users AS users".(empty($user_query)?"":" WHERE users.status = 'active' AND (".$user_query.")");
		$totalrows = intval($wpdb->get_var($sqlQuery));
		
		//total number of new signups in the time range
		$sqlQuery = "SELECT COUNT(*) FROM $wpdb->pmpro_memberships_users AS users WHERE".(empty($user_query)?"":" (".$user_query.") AND")." UNIX_TIMESTAMP(users.startdate) BETWEEN ".strtotime($s)." AND ".strtotime($e);
		$newsignups = intval($wpdb->get_var($sqlQuery));*/
	?>
	<?php /*<p class="clear"><?php echo strval($totalrows)?> relevant members found. These members have a lifetime value of <strong>$<?php echo(number_format(pmpro_reporting_get_lifetime_value(array("level" => $l))))?></strong>. During this period, <strong><?php echo(number_format($newsignups))?> new members</strong> joined.</p> */ ?>

	<div class="options-box">
		<div class="options-row">
			<ul>
				<li>
					Level: <select name="l" onchange="jQuery('#posts-filter').submit();">
						<option value="" <?php if(!$l) { ?>selected="selected"<?php } ?>>Any Level</option>
						<?php
							$levels = $wpdb->get_results("SELECT id, name FROM $wpdb->pmpro_membership_levels");
							foreach($levels as $level)
							{
							?>
								<option value="<?php echo $level->id?>" <?php if($l == $level->id) { ?>selected="selected"<?php } ?>><?php echo($level->name); ?></option>
							<?php
							}
						?>
					</select>
				</li>
				<li>
					Affiliate: <select name="a" onchange="jQuery('#posts-filter').submit();">
						<option value="" <?php if(!$a) { ?>selected="selected"<?php } ?>>Any Affiliate</option>
						<?php
							$affiliates = $wpdb->get_results("SELECT DISTINCT affiliate_id as id FROM $wpdb->pmpro_membership_orders");
							foreach($affiliates as $affiliate)
							{
								if(empty($affiliate->id)){continue;}
							?>
								<option value="<?php echo $affiliate->id?>" <?php if($a == $affiliate->id) { ?>selected="selected"<?php } ?>><?php echo($affiliate->id); ?></option>
							<?php
							}
						?>
					</select>
				</li>
				<li>
					Coupon: <select name="c" onchange="jQuery('#posts-filter').submit();">
						<option value="" <?php if(!$c) { ?>selected="selected"<?php } ?>>Any Coupon</option>
						<?php
							$coupons = $wpdb->get_results("SELECT id, code as name FROM $wpdb->pmpro_discount_codes");
							foreach($coupons as $coupon)
							{
							?>
								<option value="<?php echo $coupon->id?>" <?php if($c == $coupon->id) { ?>selected="selected"<?php } ?>><?php echo($coupon->name); ?></option>
							<?php
							}
						?>
					</select>
				</li>
			</ul>
		</div><br>
		<div class="options-row">
			<ul>
				<li>
					Start Date: <input type="text" class="datepicker" name="s" value="<?php echo($s); ?>" onchange="jQuery('#posts-filter').submit();">
				</li>
				<li>
					End Date: <input type="text" class="datepicker" name="e" value="<?php echo($e); ?>" onchange="jQuery('#posts-filter').submit();">
				</li>
				<li>
					Period: <select name="p" onchange="jQuery('#posts-filter').submit();">
						<option value="" <?php if(!$p){echo('selected="selected"');}?>>Monthly</option>
						<option value="d" <?php if($p == "d"){echo('selected="selected"');}?>>Daily</option>
					</select>
				</li>
			</ul>
		</div><br>
		<div class="options-row">
			<ul>
				<li>
					Show: 
					<input type="checkbox" name="o[tmem]" <?php if(isset($o['tmem'])){echo('checked="checked"');}?> onchange="jQuery('#posts-filter').submit();"> Total Members 
					<input type="checkbox" name="o[nmem]" <?php if(isset($o['nmem'])){echo('checked="checked"');}?> onchange="jQuery('#posts-filter').submit();"> New Members 
					<input type="checkbox" name="o[cmem]" <?php if(isset($o['cmem'])){echo('checked="checked"');}?> onchange="jQuery('#posts-filter').submit();"> Cancelled Members 
				</li>
			</ul>
		</div>
		<div style="clear:both"></div>
	</div>

	<script type='text/javascript'>jQuery(document).ready(function(){jQuery(".datepicker").datepicker();});</script>

	<?php pmpro_reporting_initialize_google_charts(); ?>

	<?php
		$periods = "monthly";
		if($p == "d"){$periods = "daily";}

		$graph = array();
		if(isset($o['tmem'])){$graph['Total Members'] = pmpro_reporting_get_total_members(array("time_start" => strtotime($s), "time_end" => strtotime($e), "periods" => $periods, "level" => $l, "affiliate" => $a, "coupon" => $c));}
		if(isset($o['nmem'])){$graph['New Members'] = pmpro_reporting_get_new_members(array("time_start" => strtotime($s), "time_end" => strtotime($e), "periods" => $periods, "level" => $l, "affiliate" => $a, "coupon" => $c));}
		if(isset($o['cmem'])){$graph['Cancelled Members'] = pmpro_reporting_get_cancelled_members(array("time_start" => strtotime($s), "time_end" => strtotime($e), "periods" => $periods, "level" => $l, "affiliate" => $a, "coupon" => $c));}
		
		pmpro_reporting_render_google_chart("Graph", $graph, '["blue","green","red"]');
	?>
	
	</form>
	
</div>