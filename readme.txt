=== PMPro Reporting ===
Contributors: zookatron
Tags: pmpro, paid memberships pro

Adds detailed revenue and membership reporting pages to Paid Memberships Pro.

== Changelog ==

= 1.2 =
* Bug fixes and code cleanup.

= 1.0 =
* Initial release.
