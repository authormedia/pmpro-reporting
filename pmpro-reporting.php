<?php
/*
Plugin Name: PMPro Reporting
Description: Adds a detailed reporting page to Paid Memberships Pro.
Version: 1.2
Author: Tim Zook
Author URI: http://www.timzook.tk
*/

//require Paid Memberships Pro
function pmpro_reporting_dependencies()
{
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	if(function_exists("is_plugin_active"))
	{
		if(!is_plugin_active('paid-memberships-pro/paid-memberships-pro.php'))
		{
			$plugin = plugin_basename(__FILE__);
			deactivate_plugins($plugin);
		}
	}
}
add_action("init", "pmpro_reporting_dependencies");

//add reporting page to admin
function pmpro_reporting_add_pages()
{
	$page = add_submenu_page('pmpro-membershiplevels', 'Revenue Reporting', 'Revenue Reporting', 'manage_options', 'pmpro-revenue-reporting', 'pmpro_revenue_reporting_adminpage');
	add_action('admin_print_styles-'.$page, 'pmpro_reporting_enqueue');
	$page = add_submenu_page('pmpro-membershiplevels', 'Membership Reporting', 'Members Reporting', 'manage_options', 'pmpro-members-reporting', 'pmpro_membership_reporting_adminpage');
	add_action('admin_print_styles-'.$page, 'pmpro_reporting_enqueue');
}
add_action('admin_menu', 'pmpro_reporting_add_pages', 20);

//render reporting page
function pmpro_revenue_reporting_adminpage()
{
	require_once(dirname(__FILE__) . "/adminpages/revenue-reporting.php");
}

//render reporting page
function pmpro_membership_reporting_adminpage()
{
	require_once(dirname(__FILE__) . "/adminpages/members-reporting.php");
}

//enqueue javascript and css
function pmpro_reporting_enqueue()
{
	wp_register_style('pmpro-reporting-style', plugins_url('style.css', __FILE__));
    wp_enqueue_style('pmpro-reporting-style');
	wp_enqueue_script("jquery");
	wp_enqueue_script("jquery-ui-core");
	wp_enqueue_script("jquery-ui-datepicker");
}

//funcs
function pmpro_reporting_initialize_google_charts()
{
	echo('<script type="text/javascript" src="https://www.google.com/jsapi"></script><script type="text/javascript">google.load("visualization", "1.0", {"packages":["corechart"]});</script>');
}

//todo: clean up chart render functions
function pmpro_reporting_render_google_chart($name, $data, $colors)
{
	$width = 1000; $height = 300;

	if(!is_array($data) or count($data) < 1){return;}

	$data_array = array();
	foreach($data[key($data)] as $key=>$value)
	{
		$datum = array('"'.date("M j", $key).'"');
		foreach($data as $desc=>$stuff) {
			$datum[] = intval($stuff[$key]);
		}
		$data_array[] = '['.implode(",", $datum).']';
	}
	$data_string = implode(",", $data_array);

	$rownames = "";
	foreach($data as $desc=>$stuff) {
		$rownames .= 'data.addColumn("number", "'.$desc.'");';
	}
	echo('<div id="chart_div_'.urlencode($name).'" style="width:'.$width.'px; height:'.$height.'px"></div>
		<script type="text/javascript">
			var data = new google.visualization.DataTable();
			data.addColumn("string", "Time");'.$rownames.'
			data.addRows(['.$data_string.']);

			var options = {"title":"'.$name.'", "width":'.$width.', "height":'.$height.', "legend":"none", colors:'.$colors.'};
			var chart = new google.visualization.ColumnChart(document.getElementById("chart_div_'.urlencode($name).'"));
			chart.draw(data, options);
		</script>');
}

function pmpro_reporting_sync_mysql_timezone()
{
	global $wpdb;
	$now = new DateTime();
	$mins = $now->getOffset() / 60;
	$sgn = ($mins < 0 ? -1 : 1);
	$mins = abs($mins);
	$hrs = floor($mins / 60);
	$mins -= $hrs * 60;
	$offset = sprintf('%+d:%02d', $hrs*$sgn, $mins);
	$wpdb->query("SET time_zone = '".$offset."';");
}

function pmpro_reporting_calculate_predicted_revenue($args = array())
{
	global $wpdb;
	extract(wp_parse_args($args, array()), EXTR_SKIP);

	//make sure time zone is correct or timestamps might not line up!
	pmpro_reporting_sync_mysql_timezone();

	//sanitize dates
	if(!isset($time_start) || !isset($time_end)){return;}
	$time_start = is_string($time_start) ? strtotime($time_start) : floor($time_start);
	$time_end = is_string($time_end) ? strtotime($time_end) : floor($time_end);

	//factor in query
	$user_query = (isset($level) and $level!==false) ? "users.membership_id = ".intval($level) : "";
	$user_query .= (isset($affiliate) and $affiliate!==false) ? (empty($user_query)?"":" AND ")."(SELECT meta_value FROM $wpdb->usermeta WHERE meta_key = 'pmpro_affiliate_id' AND user_id = users.user_id) = ".intval($affiliate) : "";
	$user_query .= (isset($coupon) and $coupon!==false) ? (empty($user_query)?"":" AND ")."users.code_id = ".intval($coupon) : "";

	//todo: make query factor in expiration dates
	//get data for relevant members
	$sqlQuery = "SELECT
					users.user_id AS user_id,
					users.billing_amount AS billing_amount,
					users.trial_amount AS trial_amount,
					temp.billing_period AS period,
					CASE WHEN (temp.billing_period+temp.signup > ".$time_start."+temp.billing_period-((".$time_start."-(temp.billing_period+temp.signup))%temp.billing_period) OR ".$time_start." < temp.billing_period+temp.signup) THEN temp.billing_period+temp.signup ELSE ".$time_start."+temp.billing_period-((".$time_start."-(temp.billing_period+temp.signup))%temp.billing_period) END AS user_start,
					CASE WHEN (".$time_end." < temp.signup+temp.billing_period*users.billing_limit OR users.billing_limit = 0) THEN ".$time_end." ELSE temp.signup+temp.billing_period*users.billing_limit END AS user_end,
					temp.signup+temp.billing_period*users.trial_limit AS trial_end 
				FROM
					$wpdb->pmpro_memberships_users AS users
						JOIN
							(SELECT user_id, CASE cycle_period WHEN 'Day' THEN 86400 WHEN 'Week' THEN 604800 WHEN 'Month' THEN 2629743 WHEN 'Year' THEN 31556926 ELSE 0 END AS billing_period, UNIX_TIMESTAMP(startdate) AS signup FROM $wpdb->pmpro_memberships_users) AS temp
						ON users.user_id = temp.user_id
				WHERE
					users.status = 'active'
					AND users.billing_amount > 0 AND temp.signup+temp.billing_period <= ".$time_end."
					AND (temp.signup+temp.billing_period*users.billing_limit >= ".$time_start." OR users.billing_limit = 0)".(empty($user_query)?"":" AND (".$user_query.")");
	$people = $wpdb->get_results($sqlQuery, ARRAY_A);

	$revenue = 0;

	if(isset($periods))
	{
		$revenue = array();

		if(strtolower($periods) == "monthly")
		{
			//Initialize with empty array
			$month_start = idate("Y", $time_start)*12+idate("m", $time_start); $month_end = idate("Y", $time_end)*12+idate("m", $time_end);
			for($i = $month_start; $i <= $month_end; $i++){$revenue[mktime(0, 0, 0, intval($i%12), 1, intval($i/12))] = 0;}

			//Add in initial payments
			$sqlQuery = "SELECT SUM(users.initial_payment) as initial_payment, users.startdate as timestamp, UNIX_TIMESTAMP(DATE_FORMAT(users.startdate, '%Y/%m/01')) AS array_index FROM $wpdb->pmpro_memberships_users AS users WHERE".(empty($user_query)?"":" (".$user_query.") AND")." users.status = 'active' AND UNIX_TIMESTAMP(startdate) BETWEEN ".$time_start." AND ".$time_end." GROUP BY array_index";
			$initial_payments = $wpdb->get_results($sqlQuery, ARRAY_A);
			foreach($initial_payments as $payment)
			{
				$revenue[intval(strtotime(date("m/01/Y 00:00:00", strtotime($payment['timestamp']))))] += intval($payment['initial_payment']);
			}

			//Add in recurring payments
			foreach($people as $person)
			{
				for($i = $person['user_start']; $i <= $person['user_end']; $i += $person['period'])
				{
					$index = strtotime(date("m/1/Y", $i));
					$revenue[$index] += ($i <= $person['trial_end'] ? $person['trial_amount'] : $person['billing_amount']);
				}
			}
		}
		else if(strtolower($periods) == "daily")
		{
			//Initialize with empty array
			for($i = strtotime(date("m/d/Y", $time_start)); $i <= $time_end; $i = mktime(0, 0, 0, idate("m", $i), idate("d", $i)+1, idate("Y", $i))){$revenue[$i] = 0;}

			//Add in initial payments
			$sqlQuery = "SELECT SUM(users.initial_payment) as initial_payment, users.startdate as timestamp, UNIX_TIMESTAMP(DATE(users.startdate)) AS array_index FROM $wpdb->pmpro_memberships_users AS users WHERE".(empty($user_query)?"":" (".$user_query.") AND")." users.status = 'active' AND UNIX_TIMESTAMP(startdate) BETWEEN ".$time_start." AND ".$time_end." GROUP BY array_index";
			$initial_payments = $wpdb->get_results($sqlQuery, ARRAY_A);
			foreach($initial_payments as $payment)
			{
				$revenue[intval(strtotime(date("m/d/Y 00:00:00", strtotime($payment['timestamp']))))] += intval($payment['initial_payment']);
			}

			//Add in recurring payments
			foreach($people as $person)
			{
				for($i = $person['user_start']; $i <= $person['user_end']; $i += $person['period'])
				{
					$index = strtotime(date("m/d/Y", $i));
					$revenue[$index] += ($i <= $person['trial_end'] ? $person['trial_amount'] : $person['billing_amount']);
				}
			}
		}
	}
	else
	{
		//Initialize with inital payments
		$sqlQuery = "SELECT SUM(users.initial_payment) FROM $wpdb->pmpro_memberships_users AS users WHERE".(empty($user_query)?"":" (".$user_query.") AND")." users.status = 'active' AND UNIX_TIMESTAMP(startdate) BETWEEN ".$time_start." AND ".$time_end;
		$initial_revenue = $wpdb->get_var($sqlQuery);
		$revenue = intval($initial_revenue);

		//Add in recurring payments
		foreach($people as $person)
		{
			for($i = $person['user_start']; $i <= $person['user_end']; $i += $person['period'])
			{
				$revenue += ($i <= $person['trial_end'] ? $person['trial_amount'] : $person['billing_amount']);
			}
		}
	}

	return $revenue;
}

function pmpro_reporting_calculate_actual_revenue($args = array())
{
	global $wpdb;
	extract(wp_parse_args($args, array()), EXTR_SKIP);

	//make sure time zone is correct or timestamps might not line up!
	pmpro_reporting_sync_mysql_timezone();

	//factor in query
	$user_query = (isset($level) and $level!==false) ? "orders.membership_id = ".intval($level) : "";
	$user_query .= (isset($affiliate) and $affiliate!==false) ? (empty($user_query)?"":" AND ")."orders.affiliate_id = ".intval($affiliate) : "";
	$user_query .= (isset($coupon) and $coupon!==false) ? (empty($user_query)?"":" AND ")."(SELECT code_id FROM $wpdb->pmpro_memberships_users WHERE status = 'active' AND user_id = orders.user_id) = ".intval($coupon) : "";

	$revenue = 0;

	if(isset($periods))
	{
		$revenue = array();

		if(strtolower($periods) == "monthly")
		{
			//Initialize with empty array
			$month_start = idate("Y", $time_start)*12+idate("m", $time_start); $month_end = idate("Y", $time_end)*12+idate("m", $time_end);
			for($i = $month_start; $i <= $month_end; $i++){$revenue[mktime(0, 0, 0, intval($i%12), 1, intval($i/12))] = 0;}

			//Get the total revenue for each month
			$sqlQuery = "SELECT UNIX_TIMESTAMP(DATE_FORMAT(orders.timestamp, '%Y/%m/01 00:00:00')) AS array_index, orders.timestamp as timestamp, SUM(orders.total) AS total FROM $wpdb->pmpro_membership_orders AS orders WHERE".(empty($user_query)?"":" (".$user_query.") AND")." (orders.status = 'success' OR orders.status = '') AND UNIX_TIMESTAMP(orders.timestamp) BETWEEN ".floor($time_start)." AND ".floor($time_end)." GROUP BY array_index";
			$months = $wpdb->get_results($sqlQuery, ARRAY_A);
			foreach($months as $month)
			{
				$revenue[intval(strtotime(date("m/01/Y 00:00:00", strtotime($month['timestamp']))))] = intval($month['total']);
			}
		}
		else if(strtolower($periods) == "daily")
		{
			//Initialize with empty array
			for($i = strtotime(date("m/d/Y", $time_start)); $i <= $time_end; $i = mktime(0, 0, 0, idate("m", $i), idate("d", $i)+1, idate("Y", $i))){$revenue[$i] = 0;}

			//Get the total revenue for each day
			$sqlQuery = "SELECT UNIX_TIMESTAMP(DATE(orders.timestamp)) AS array_index, orders.timestamp as timestamp, SUM(orders.total) AS total FROM $wpdb->pmpro_membership_orders AS orders WHERE".(empty($user_query)?"":" (".$user_query.") AND")." (orders.status = 'success' OR orders.status = '') AND UNIX_TIMESTAMP(orders.timestamp) BETWEEN ".floor($time_start)." AND ".floor($time_end)." GROUP BY array_index";
			$days = $wpdb->get_results($sqlQuery, ARRAY_A);
			foreach($days as $day)
			{
				$revenue[intval(strtotime(date("m/d/Y 00:00:00", strtotime($day['timestamp']))))] = intval($day['total']);
			}
		}
	}
	else
	{
		$sqlQuery = "SELECT SUM(orders.total) FROM $wpdb->pmpro_membership_orders AS orders WHERE".(empty($user_query)?"":" (".$user_query.") AND")." (orders.status = 'success' OR orders.status = '') AND UNIX_TIMESTAMP(orders.timestamp) BETWEEN ".floor($time_start)." AND ".floor($time_end);
		$revenue = intval($wpdb->get_var($sqlQuery));
	}
	
	return $revenue;
}

function pmpro_reporting_get_total_members($args = array())
{
	global $wpdb;
	extract(wp_parse_args($args, array()), EXTR_SKIP);

	//make sure time zone is correct or timestamps might not line up!
	pmpro_reporting_sync_mysql_timezone();

	//factor in query
	$user_query = (isset($level) and $level!==false) ? "users.membership_id = ".intval($level) : "";
	$user_query .= (isset($affiliate) and $affiliate!==false) ? (empty($user_query)?"":" AND ")."(SELECT meta_value FROM $wpdb->usermeta WHERE meta_key = 'pmpro_affiliate_id' AND user_id = users.user_id) = ".intval($affiliate) : "";
	$user_query .= (isset($coupon) and $coupon!==false) ? (empty($user_query)?"":" AND ")."users.code_id = ".intval($coupon) : "";

	$members = 0;

	if(isset($periods))
	{
		$members = array();

		if(strtolower($periods) == "monthly")
		{
			//initialize total with previous memberships
			$sqlQuery = "SELECT COUNT(*) FROM $wpdb->pmpro_memberships_users AS users WHERE".(empty($user_query)?"":" (".$user_query.") AND")." UNIX_TIMESTAMP(users.startdate) < ".floor($time_start)." AND users.startdate IS NOT NULL AND users.startdate > 0 AND NOT (UNIX_TIMESTAMP(users.enddate) <= ".floor($time_start)." AND users.enddate IS NOT NULL AND users.enddate > 0)";
			$total = intval($wpdb->get_var($sqlQuery));
			$months = array();

			//Get the total new members for each month
			$sqlQuery = "SELECT UNIX_TIMESTAMP(DATE_FORMAT(users.startdate, '%Y/%m/01')) AS array_index, users.startdate as timestamp, COUNT(*) AS total FROM $wpdb->pmpro_memberships_users AS users WHERE".(empty($user_query)?"":" (".$user_query.") AND")." UNIX_TIMESTAMP(users.startdate) BETWEEN ".floor($time_start)." AND ".floor($time_end)." GROUP BY array_index";
			$new_months = $wpdb->get_results($sqlQuery, ARRAY_A);
			foreach($new_months as $month){$months[intval(strtotime(date("m/01/Y 00:00:00", strtotime($month['timestamp']))))] = intval($month['total']);}

			//Get the total cancelled members for each month
			$sqlQuery = "SELECT UNIX_TIMESTAMP(DATE_FORMAT(users.enddate, '%Y/%m/01')) AS array_index, users.enddate as timestamp, COUNT(*) AS total FROM $wpdb->pmpro_memberships_users AS users WHERE".(empty($user_query)?"":" (".$user_query.") AND")." users.startdate IS NOT NULL AND users.startdate > 0 AND UNIX_TIMESTAMP(users.enddate) BETWEEN ".floor($time_start)." AND ".floor($time_end)." GROUP BY array_index";
			$new_months = $wpdb->get_results($sqlQuery, ARRAY_A);
			foreach($new_months as $month){$months[intval(strtotime(date("m/01/Y 00:00:00", strtotime($month['timestamp']))))] -= intval($month['total']);}
	
			//Add them up
			$month_start = idate("Y", $time_start)*12+idate("m", $time_start); $month_end = idate("Y", $time_end)*12+idate("m", $time_end);
			for($i = $month_start; $i <= $month_end; $i++)
			{
				$time = mktime(0, 0, 0, intval($i%12), 1, intval($i/12));
				if(isset($months[$time])){$total += intval($months[$time]);}
				$members[$time] = $total;
			}
		}
		else if(strtolower($periods) == "daily")
		{
			//initialize total with previous memberships
			$sqlQuery = "SELECT COUNT(*) FROM $wpdb->pmpro_memberships_users AS users WHERE".(empty($user_query)?"":" (".$user_query.") AND")." UNIX_TIMESTAMP(users.startdate) < ".floor($time_start)." AND users.startdate IS NOT NULL AND users.startdate > 0 AND NOT (UNIX_TIMESTAMP(users.enddate) <= ".floor($time_start)." AND users.enddate IS NOT NULL AND users.enddate > 0)";
			$total = intval($wpdb->get_var($sqlQuery));
			$days = array();

			//Get the total new members for each day
			$sqlQuery = "SELECT UNIX_TIMESTAMP(DATE(users.startdate)) AS array_index, users.startdate as timestamp, COUNT(*) AS total FROM $wpdb->pmpro_memberships_users AS users WHERE".(empty($user_query)?"":" (".$user_query.") AND")." UNIX_TIMESTAMP(users.startdate) BETWEEN ".floor($time_start)." AND ".floor($time_end)." GROUP BY array_index";
			$new_days = $wpdb->get_results($sqlQuery, ARRAY_A);
			foreach($new_days as $day){$days[intval(strtotime(date("m/d/Y 00:00:00", strtotime($day['timestamp']))))] = intval($day['total']);}

			//Get the total cancelled members for each day
			$sqlQuery = "SELECT UNIX_TIMESTAMP(DATE(users.enddate)) AS array_index, users.enddate as timestamp, COUNT(*) AS total FROM $wpdb->pmpro_memberships_users AS users WHERE".(empty($user_query)?"":" (".$user_query.") AND")." users.startdate IS NOT NULL AND users.startdate > 0 AND UNIX_TIMESTAMP(users.enddate) BETWEEN ".floor($time_start)." AND ".floor($time_end)." GROUP BY array_index";
			$new_days = $wpdb->get_results($sqlQuery, ARRAY_A);
			foreach($new_days as $day){$days[intval(strtotime(date("m/d/Y 00:00:00", strtotime($day['timestamp']))))] -= intval($day['total']);}

			//Add them up
			for($i = strtotime(date("m/d/Y", $time_start)); $i <= $time_end; $i = mktime(0, 0, 0, idate("m", $i), idate("d", $i)+1, idate("Y", $i)))
			{
				if(isset($days[$i])){$total += intval($days[$i]);}
				$members[$i] = $total;
			}
		}
	}
	else
	{
		$sqlQuery = "SELECT COUNT(*) FROM $wpdb->pmpro_memberships_users AS users WHERE".(empty($user_query)?"":" (".$user_query.") AND")." UNIX_TIMESTAMP(users.startdate) < ".floor($time_end);
		$members = intval($wpdb->get_var($sqlQuery));
	}
	
	return $members;
}

function pmpro_reporting_get_new_members($args = array())
{
	global $wpdb;
	extract(wp_parse_args($args, array()), EXTR_SKIP);

	//make sure time zone is correct or timestamps might not line up!
	pmpro_reporting_sync_mysql_timezone();

	//factor in query
	$user_query = (isset($level) and $level!==false) ? "users.membership_id = ".intval($level) : "";
	$user_query .= (isset($affiliate) and $affiliate!==false) ? (empty($user_query)?"":" AND ")."(SELECT meta_value FROM $wpdb->usermeta WHERE meta_key = 'pmpro_affiliate_id' AND user_id = users.user_id) = ".intval($affiliate) : "";
	$user_query .= (isset($coupon) and $coupon!==false) ? (empty($user_query)?"":" AND ")."users.code_id = ".intval($coupon) : "";

	$members = 0;

	if(isset($periods))
	{
		$members = array();

		if(strtolower($periods) == "monthly")
		{
			//Get the total new members for each month
			$months = array();
			$sqlQuery = "SELECT UNIX_TIMESTAMP(DATE_FORMAT(users.startdate, '%Y/%m/01')) AS array_index, users.startdate as timestamp, COUNT(*) AS total FROM $wpdb->pmpro_memberships_users AS users WHERE".(empty($user_query)?"":" (".$user_query.") AND")." UNIX_TIMESTAMP(users.startdate) BETWEEN ".floor($time_start)." AND ".floor($time_end)." GROUP BY array_index";
			$new_months = $wpdb->get_results($sqlQuery, ARRAY_A);
			foreach($new_months as $month){$months[intval(strtotime(date("m/01/Y 00:00:00", strtotime($month['timestamp']))))] = intval($month['total']);}

			//Extract data
			$month_start = idate("Y", $time_start)*12+idate("m", $time_start); $month_end = idate("Y", $time_end)*12+idate("m", $time_end);
			for($i = $month_start; $i <= $month_end; $i++)
			{
				$time = mktime(0, 0, 0, intval($i%12), 1, intval($i/12));
				$members[$time] = isset($months[$time]) ? intval($months[$time]) : 0;
			}
		}
		else if(strtolower($periods) == "daily")
		{
			//Get the total new members for each day
			$days = array();
			$sqlQuery = "SELECT UNIX_TIMESTAMP(DATE(users.startdate)) AS array_index, users.startdate as timestamp, COUNT(*) AS total FROM $wpdb->pmpro_memberships_users AS users WHERE".(empty($user_query)?"":" (".$user_query.") AND")." UNIX_TIMESTAMP(users.startdate) BETWEEN ".floor($time_start)." AND ".floor($time_end)." GROUP BY array_index";
			$new_days = $wpdb->get_results($sqlQuery, ARRAY_A);
			foreach($new_days as $day){$days[intval(strtotime(date("m/d/Y 00:00:00", strtotime($day['timestamp']))))] = intval($day['total']);}
			
			//Extract data
			for($i = strtotime(date("m/d/Y", $time_start)); $i <= $time_end; $i = mktime(0, 0, 0, idate("m", $i), idate("d", $i)+1, idate("Y", $i)))
			{
				$members[$i] = isset($days[$i]) ? intval($days[$i]) : 0;
			}
		}
	}
	else
	{
		$sqlQuery = "SELECT COUNT(*) FROM $wpdb->pmpro_memberships_users AS users WHERE".(empty($user_query)?"":" (".$user_query.") AND")." UNIX_TIMESTAMP(users.startdate) > ".floor($time_start)." AND UNIX_TIMESTAMP(users.startdate) < ".floor($time_end);
		$members = intval($wpdb->get_var($sqlQuery));
	}
	
	return $members;
}

function pmpro_reporting_get_cancelled_members($args = array())
{
	global $wpdb;
	extract(wp_parse_args($args, array()), EXTR_SKIP);

	//make sure time zone is correct or timestamps might not line up!
	pmpro_reporting_sync_mysql_timezone();

	//factor in query
	$user_query = (isset($level) and $level!==false) ? "users.membership_id = ".intval($level) : "";
	$user_query .= (isset($affiliate) and $affiliate!==false) ? (empty($user_query)?"":" AND ")."(SELECT meta_value FROM $wpdb->usermeta WHERE meta_key = 'pmpro_affiliate_id' AND user_id = users.user_id) = ".intval($affiliate) : "";
	$user_query .= (isset($coupon) and $coupon!==false) ? (empty($user_query)?"":" AND ")."users.code_id = ".intval($coupon) : "";

	$members = 0;

	if(isset($periods))
	{
		$members = array();

		if(strtolower($periods) == "monthly")
		{
			//Get the total cancelled members for each month
			$months = array();
			$sqlQuery = "SELECT UNIX_TIMESTAMP(DATE_FORMAT(users.enddate, '%Y/%m/01')) AS array_index, users.enddate as timestamp, COUNT(*) AS total FROM $wpdb->pmpro_memberships_users AS users WHERE".(empty($user_query)?"":" (".$user_query.") AND")." users.enddate IS NOT NULL AND UNIX_TIMESTAMP(users.enddate) BETWEEN ".floor($time_start)." AND ".floor($time_end)." GROUP BY array_index";
			$new_months = $wpdb->get_results($sqlQuery, ARRAY_A);
			foreach($new_months as $month){$months[intval(strtotime(date("m/01/Y 00:00:00", strtotime($month['timestamp']))))] = intval($month['total']);}

			//Extract data
			$month_start = idate("Y", $time_start)*12+idate("m", $time_start); $month_end = idate("Y", $time_end)*12+idate("m", $time_end);
			for($i = $month_start; $i <= $month_end; $i++)
			{
				$time = mktime(0, 0, 0, intval($i%12), 1, intval($i/12));
				$members[$time] = isset($months[$time]) ? intval($months[$time]) : 0;
			}
		}
		else if(strtolower($periods) == "daily")
		{
			//Get the total cancelled members for each day
			$days = array();
			$sqlQuery = "SELECT UNIX_TIMESTAMP(DATE(users.enddate)) AS array_index, users.enddate as timestamp, COUNT(*) AS total FROM $wpdb->pmpro_memberships_users AS users WHERE".(empty($user_query)?"":" (".$user_query.") AND")." users.enddate IS NOT NULL AND UNIX_TIMESTAMP(users.enddate) BETWEEN ".floor($time_start)." AND ".floor($time_end)." GROUP BY array_index";
			$new_days = $wpdb->get_results($sqlQuery, ARRAY_A);
			foreach($new_days as $day){$days[intval(strtotime(date("m/d/Y 00:00:00", strtotime($day['timestamp']))))] = intval($day['total']);}

			//Extract data
			for($i = strtotime(date("m/d/Y", $time_start)); $i <= $time_end; $i = mktime(0, 0, 0, idate("m", $i), idate("d", $i)+1, idate("Y", $i)))
			{
				$members[$i] = isset($days[$i]) ? intval($days[$i]) : 0;
			}
		}
	}
	else
	{
		$sqlQuery = "SELECT COUNT(*) FROM $wpdb->pmpro_memberships_users AS users WHERE".(empty($user_query)?"":" (".$user_query.") AND")." UNIX_TIMESTAMP(users.enddate) > ".floor($time_start)." AND UNIX_TIMESTAMP(users.enddate) < ".floor($time_end);
		$members = intval($wpdb->get_var($sqlQuery));
	}
	
	return $members;
}

function pmpro_reporting_get_lifetime_value($args = array())
{
	global $wpdb;
	extract(wp_parse_args($args, array()), EXTR_SKIP);

	$user_query = (isset($level) and $level!==false) ? "orders.membership_id = ".$level : "";

	//get median of the total revenue from a user
	$sqlQuery = "SELECT COUNT(*) FROM (SELECT SUM(orders.total) AS user_total FROM $wpdb->pmpro_membership_orders AS orders WHERE ".(empty($user_query)?"":"(".$user_query.") AND ")."(orders.status = 'success' OR orders.status = '') GROUP BY orders.user_id) AS x";
	$num_rows = $wpdb->get_var($sqlQuery);
	$sqlQuery = "SELECT SUM(orders.total) AS user_total FROM $wpdb->pmpro_membership_orders AS orders WHERE ".(empty($user_query)?"":"(".$user_query.") AND ")."(orders.status = 'success' OR orders.status = '') GROUP BY orders.user_id ORDER BY user_total LIMIT ".intval($num_rows/2).",1";
	$medianvalue = $wpdb->get_var($sqlQuery);

	//get mean of the total revenue from a user
	$sqlQuery = "SELECT AVG(x.user_total) FROM (SELECT SUM(orders.total) AS user_total FROM $wpdb->pmpro_membership_orders AS orders WHERE ".(empty($user_query)?"":"(".$user_query.") AND ")."(orders.status = 'success' OR orders.status = '') GROUP BY orders.user_id) AS x";
	$meanvalue = $wpdb->get_var($sqlQuery);

	//Get the average of the mean and median... kind of a weird statistic
	$lifetimevalue = ($medianvalue+$meanvalue)/2;

	return $lifetimevalue;
}